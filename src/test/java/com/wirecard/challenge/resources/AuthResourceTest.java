package com.wirecard.challenge.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AuthResourceTest {

	@InjectMocks
	private AuthResource controller;

	@Mock
	private UserService service;

	@Autowired
	TestRestTemplate testRestTemplate;

	private static String token = "";

	@Before
	public void logar() {

	}

	@Before
	public void setUp() {
		login("admin", "123");
	}

	private void login(String email, String password) {

		String jsonLogin = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}";

		token = testRestTemplate.postForEntity("/login", jsonLogin, String.class).getHeaders()
				.getFirst("Authorization");

		testRestTemplate.getRestTemplate().setInterceptors(Collections.singletonList((request, body, execution) -> {

			request.getHeaders().add("Authorization", token);
			return execution.execute(request, body);
		}));
	}

	@Test
	public void refreshToken() throws Exception {
		ResponseEntity<String> response = testRestTemplate.postForEntity("/auth/refresh_token", "", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(response.getHeaders().getFirst("Authorization")).isNotEmpty();
	}

}

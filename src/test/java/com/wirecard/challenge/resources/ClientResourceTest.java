package com.wirecard.challenge.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.services.PaymentService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ClientResourceTest {

	@InjectMocks
	private PaymentResource controller;

	@Mock
	private PaymentService paymentService;

	@Autowired
	TestRestTemplate testRestTemplate;

	private static String token = "";

	@Before
	public void setUp() {

		login("admin", "123");
	}

	private void login(String email, String password) {
		
		String jsonLogin = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}";

		token = testRestTemplate.postForEntity("/login",jsonLogin, String.class).getHeaders()
				.getFirst("Authorization");

		testRestTemplate.getRestTemplate().setInterceptors(Collections.singletonList((request, body, execution) -> {

			request.getHeaders().add("Authorization", token);
			return execution.execute(request, body);
		}));
	}

	@Test
	public void listClients() throws Exception {
		ResponseEntity<Client[]> response = testRestTemplate.getForEntity("/clients", Client[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNotNull(response.getBody());
	}

	@Test
	public void getClient() throws Exception {
		ResponseEntity<Client> response = testRestTemplate.getForEntity("/clients/1", Client.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNotNull(response.getBody());
	}

	@Test
	public void getClientWithException404() throws Exception {
		ResponseEntity<Client> response = testRestTemplate.getForEntity("/clients/0", Client.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void getClientWithException400() throws Exception {
		ResponseEntity<Client> response = testRestTemplate.getForEntity("/clients/w", Client.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

}

package com.wirecard.challenge.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Buyer;
import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.services.PaymentService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PaymentResourceTest {

	@InjectMocks
	private PaymentResource controller;

	@Mock
	private PaymentService paymentService;

	@Autowired
	TestRestTemplate testRestTemplate;

	private static String token = "";

	@Before
	public void setUp() {

		login("admin", "123");
	}

	private void login(String email, String password) {

		String jsonLogin = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}";

		token = testRestTemplate.postForEntity("/login", jsonLogin, String.class).getHeaders()
				.getFirst("Authorization");

		testRestTemplate.getRestTemplate().setInterceptors(Collections.singletonList((request, body, execution) -> {

			request.getHeaders().add("Authorization", token);
			return execution.execute(request, body);
		}));
	}

	@Test
	public void listAll() throws Exception {
		ResponseEntity<Payment[]> response = testRestTemplate.getForEntity("/payments", Payment[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNotNull(response.getBody());
	}

	@Test
	public void listAllMyPayments() throws Exception {
		ResponseEntity<Payment[]> response = testRestTemplate.getForEntity("/payments/my", Payment[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNotNull(response.getBody());
	}

	@Test
	public void paymentStatus() throws Exception {
		ResponseEntity<Payment> response = testRestTemplate.getForEntity("/payments/14", Payment.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertNotNull(response.getBody());
	}

	@Test
	public void createPaymentWithBoleto() throws Exception {
		Payment payment = createPayment(PaymentType.TICKET);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/payments", payment, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody()).isNotEmpty();
	}

	// 400
	@Test
	public void createPaymentWithCardException400() throws Exception {
		Payment payment = createPayment(PaymentType.CREDIT_CARD);
		payment.setAmount(BigDecimal.ZERO);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/payments", payment, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(response.getBody()).isNotEmpty();
	}

	@Test
	public void createPaymentWithBoletoException400() throws Exception {
		Payment payment = createPayment(PaymentType.TICKET);
		payment.setAmount(BigDecimal.ZERO);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/payments", payment, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(response.getBody()).isNotEmpty();
	}

	@Test
	public void createPaymentWithCard() throws Exception {
		Payment payment = createPayment(PaymentType.CREDIT_CARD);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/payments", payment, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody()).isNotEmpty();
	}

	private Payment createPayment(PaymentType type) throws ParseException {
		if (type.equals(PaymentType.TICKET))
			return Payment.builder().amount(BigDecimal.valueOf(100)).buyer(getBuyer()).client(new Client(1)).type(type)
					.build();

		return Payment.builder().amount(BigDecimal.valueOf(100)).buyer(getBuyer()).client(new Client(1)).card(getCard())
				.type(type).build();
	}

	private Buyer getBuyer() {
		return Buyer.builder().cpf("51170898041").email("email@email.com").name("Name Buyer").build();
	}

	private Card getCard() throws ParseException {
		return Card.builder().cvv("012")
				.expirationDate(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().plusDays(30).toString()))
				.holderName("FULANO DE TAL").number("5160365774786687").build();
	}

}

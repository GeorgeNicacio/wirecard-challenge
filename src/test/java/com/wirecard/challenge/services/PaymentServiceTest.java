package com.wirecard.challenge.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Buyer;
import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.repositories.PaymentRepository;
import com.wirecard.challenge.resources.exception.PaymentNotFoundException;
import com.wirecard.challenge.services.exceptions.PaymentUnregisteredException;
import com.wirecard.challenge.services.exceptions.ProcessorNotFoundException;
import com.wirecard.challenge.services.exceptions.WireCardException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PaymentServiceTest {

	@InjectMocks
	private PaymentService service;
	
	@Autowired
	private ProcessorService processorService;

	@Mock
	private PaymentRepository repository;

	private Optional<Payment> paymentsReturn;

	@Before
	public void ListUp() throws ParseException {
		paymentsReturn = Optional.of(createPayment(PaymentType.CREDIT_CARD));
		service = new PaymentService(repository, processorService);
	}

	@Test
	public void findAll() throws PaymentNotFoundException, ParseException {
		List<Payment> PaymentsRetornadas = Arrays.asList(createPayment(PaymentType.CREDIT_CARD),createPayment(PaymentType.CREDIT_CARD),
														 createPayment(PaymentType.TICKET),createPayment(PaymentType.TICKET));

		when(repository.findAll()).thenReturn(PaymentsRetornadas);

		List<Payment> Payments = service.listAllPayments();

		assertNotNull(Payments);
		verify(repository, times(1)).findAll();
	}
	
	@Test
	public void listAllMyPayments() throws PaymentNotFoundException, ParseException {
		List<Payment> PaymentsRetornadas = Arrays.asList(createPayment(PaymentType.CREDIT_CARD));

		when(repository.findByUser("admin")).thenReturn(PaymentsRetornadas);

		List<Payment> Payments = service.listAllMyPayments("admin");

		assertNotNull(Payments);
		verify(repository, times(1)).findByUser("admin");
	}

	@Test
	public void findById() throws WireCardException {
		when(repository.findById(any())).thenReturn(paymentsReturn);

		Payment Payment = service.findPaymentById("1");

		assertNotNull(Payment);
		assertEquals(paymentsReturn.get(), Payment);
		verify(repository, times(1)).findById(any());
	}
	 
	@Test
	public void save() throws ProcessorNotFoundException, PaymentUnregisteredException, ParseException {
		Optional<Payment> PaymentParaSalvar = Optional.of(createPayment(PaymentType.CREDIT_CARD));

		when(repository.save(any())).thenReturn(PaymentParaSalvar);

		Optional<String> paymentSalva = service.createPayment(PaymentParaSalvar.get());

		assertNotNull(paymentSalva);
	}

	private Payment createPayment(PaymentType type) throws ParseException {
		if (type.equals(PaymentType.TICKET))
			return Payment.builder().amount(BigDecimal.valueOf(100)).buyer(getBuyer()).client(new Client(1)).type(type)
					.build();

		return Payment.builder().amount(BigDecimal.valueOf(100)).buyer(getBuyer()).client(new Client(1)).card(getCard())
				.type(type).build();
	}

	private Buyer getBuyer() {
		return Buyer.builder().cpf("51170898041").email("email@email.com").name("Name Buyer").build();
	}

	private Card getCard() throws ParseException {
		return Card.builder().cvv("012")
				.expirationDate(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().plusDays(30).toString()))
				.holderName("FULANO DE TAL").number("5160365774786687").build();
	}

}

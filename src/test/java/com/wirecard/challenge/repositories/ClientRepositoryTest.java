package com.wirecard.challenge.repositories;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Client;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ClientRepositoryTest {

	@Autowired
	private ClientRepository repository;
	
	private Client client;
	private Client client2;

	
	@Before
	public void setUp() throws Exception {
		client = Client.builder()
				.id(null)
				.build();
	
		client2 = Client.builder()
				.id(null)
				.build();
	}
	
	@Test
	public void findById() {
		Client clienteDB = repository.findById(1).get();
		assertNotNull(clienteDB);
	}
}

package com.wirecard.challenge.repositories;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Ticket;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TicketRepositoryTest {

	@Autowired
	private TicketRepository repository;
	
	private Ticket ticket; 
	
	
	@Before
	public void setUp() throws Exception {
		ticket = Ticket.builder()
				.id(null)
				.number("123013210321024")
				.build();
	}
	
	@Test
	public void findById() {
		Ticket ticketDB = repository.findById(15).get();
		assertEquals(ticket.getNumber(), ticketDB.getNumber());
	}

}

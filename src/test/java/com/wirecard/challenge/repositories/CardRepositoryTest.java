package com.wirecard.challenge.repositories;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.apache.http.client.utils.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Card;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CardRepositoryTest {

	@Autowired
	private CardRepository repository;

	private Card ca;
	private Card ca2;
	private Card ca3;

	@Before
	public void setUp() throws Exception {

		Date dataDoUsuario = new Date();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataDoUsuario);
		calendar.add(Calendar.DATE, +1);

		dataDoUsuario = calendar.getTime();

		ca = Card.builder().cvv("504").expirationDate(dataDoUsuario).holderName("FULANO DE TAL").id(null)
				.number("5160365774786687").build();
		ca2 = Card.builder().cvv("246").expirationDate(dataDoUsuario).holderName("FULANO DE TAL 2").id(null)
				.number("5490144361717293").build();
		ca3 = Card.builder().cvv("854").expirationDate(dataDoUsuario).holderName("FULANO DE TAL 3").id(null)
				.number("5475112588183036").build();
	}
	
	@Test
	public void findFirstByNumber() {
		Card CardDB = repository.findFirstByNumber("5160365774786687").get();
		assertEquals(ca.getCvv(), CardDB.getCvv());
		assertEquals(DateUtils.formatDate(ca.getExpirationDate(),"MM/YYYY"), DateUtils.formatDate(CardDB.getExpirationDate(),"MM/YYYY"));
		assertEquals(ca.getHolderName(), CardDB.getHolderName());
		assertEquals(ca.getNumber(), CardDB.getNumber());
	}
	
	@Test
	public void findById() {
		Card CardDB = repository.findById(6).get();
		assertEquals(ca2.getCvv(), CardDB.getCvv());
		assertEquals(DateUtils.formatDate(ca2.getExpirationDate(),"MM/YYYY"), DateUtils.formatDate(CardDB.getExpirationDate(),"MM/YYYY"));
		assertEquals(ca2.getHolderName(), CardDB.getHolderName());
		assertEquals(ca2.getNumber(), CardDB.getNumber());
	}

}

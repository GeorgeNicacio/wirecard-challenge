package com.wirecard.challenge.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Buyer;
import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.domain.Ticket;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PaymentRepositoryTest {

	@Autowired
	private PaymentRepository repository;

	private Payment payment;
	private Payment payment2;
	private Payment payment3;
	private Payment payment4;

	@Before
	public void setUp() throws Exception {
		
		Client client = Client.builder()
				.id(null)
				.build();
	
		Client client2 = Client.builder()
				.id(null)
				.build();
		
		Buyer george = Buyer.builder()
				.cpf("26927963065")
				.email("georgenicacio@gmail.com")
				.id(null)
				.name("George Nicacio")
				.build();
		
		Buyer fulano2 = Buyer.builder()
				.cpf("38355256034")
				.email("fulano2@gmail.com")
				.id(null)
				.name("Fulano 2")
				.build() ;
		
		Buyer fulano3 = Buyer.builder()
				.cpf("52113674050")
				.email("fulano3@gmail.com")
				.id(null)
				.name("Fulano 3")
				.build() ;
		
		Date dataDoUsuario = new Date();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataDoUsuario);
		calendar.add(Calendar.DATE, +1);

		dataDoUsuario = calendar.getTime();
		
		Card ca = Card.builder().cvv("504").expirationDate(dataDoUsuario).holderName("FULANO DE TAL").id(null)
				.number("5160365774786687").build();
		Card ca2 = Card.builder().cvv("246").expirationDate(dataDoUsuario).holderName("FULANO DE TAL 2").id(null)
				.number("5490144361717293").build();
		Card ca3 = Card.builder().cvv("854").expirationDate(dataDoUsuario).holderName("FULANO DE TAL 3").id(null)
				.number("5475112588183036").build();
		
		Ticket b4 = Ticket.builder()
				.id(null)
				.number("123013210321024")
				.build();
		
		payment = Payment.builder().amount(new BigDecimal(150)).buyer(george).card(ca).client(client).id(null)
				.status(PaymentStatus.APPROVED).type(PaymentType.CREDIT_CARD).build();

		payment2 = Payment.builder().amount(new BigDecimal(150)).buyer(fulano2).card(ca2).client(client).id(null)
				.status(PaymentStatus.CANCELED).type(PaymentType.CREDIT_CARD).build();

		payment3 = Payment.builder().amount(new BigDecimal(150)).buyer(fulano3).card(ca3).client(client).id(null)
				.status(PaymentStatus.DENIED).type(PaymentType.CREDIT_CARD).build();

		payment4 = Payment.builder().amount(new BigDecimal(150)).ticket(b4).buyer(fulano3).client(client2).id(null)
				.status(PaymentStatus.APPROVED).type(PaymentType.TICKET).build();
	}

	@Test
	public void findById() {
		Payment paymentDB = repository.findById(8).get();
		assertEquals(payment.getStatus(), paymentDB.getStatus());
		assertEquals(payment.getType(), paymentDB.getType());
		assertEquals(payment.getBuyer().getCpf(), paymentDB.getBuyer().getCpf());
		assertEquals(payment.getBuyer().getEmail(), paymentDB.getBuyer().getEmail());
		assertEquals(payment.getBuyer().getName(), paymentDB.getBuyer().getName());
		assertNotNull(paymentDB);
	}
}

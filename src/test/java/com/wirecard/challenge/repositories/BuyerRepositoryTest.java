package com.wirecard.challenge.repositories;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.wirecard.challenge.domain.Buyer;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BuyerRepositoryTest {

	@Autowired
	private BuyerRepository buyerRepository;
	
	private Buyer george;
	
	private Buyer fulano2;
	
	private Buyer fulano3;
	
	@Before
	public void setUp() throws Exception {
		george = Buyer.builder()
				.cpf("26927963065")
				.email("georgenicacio@gmail.com")
				.id(null)
				.name("George Nicacio")
				.build();
		
		fulano2 = Buyer.builder()
				.cpf("38355256034")
				.email("fulano2@gmail.com")
				.id(null)
				.name("Fulano 2")
				.build() ;
		
		fulano3 = Buyer.builder()
				.cpf("52113674050")
				.email("fulano3@gmail.com")
				.id(null)
				.name("Fulano 3")
				.build() ;
	}
	
	@Test
	public void findById() {
		Buyer buyerDB = buyerRepository.findById(9).get();
		assertEquals(george.getEmail(), buyerDB.getEmail());
		assertEquals(george.getCpf(), buyerDB.getCpf());
		assertEquals(george.getName(), buyerDB.getName());
	}

}

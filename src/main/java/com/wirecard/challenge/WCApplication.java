package com.wirecard.challenge;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WCApplication implements CommandLineRunner  {

	public static void main(String[] args) {
		SpringApplication.run(WCApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {	
	}	
}

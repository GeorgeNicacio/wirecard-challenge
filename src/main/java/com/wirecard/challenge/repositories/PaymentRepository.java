package com.wirecard.challenge.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.wirecard.challenge.domain.Payment;


@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer>{
	
	@Transactional(readOnly=true)
	@Query("SELECT DISTINCT payment "
			+ "FROM Usuario user, Payment payment "
			+ "WHERE payment.client.id = user.client.id "
			+ "AND  user.username =:username")
	public List<Payment> findByUser(@Param("username") String username);

}

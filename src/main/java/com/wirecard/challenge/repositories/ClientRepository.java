package com.wirecard.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wirecard.challenge.domain.Client;


public interface ClientRepository extends JpaRepository<Client, Integer> {
}

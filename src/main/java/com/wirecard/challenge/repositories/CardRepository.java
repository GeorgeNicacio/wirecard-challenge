package com.wirecard.challenge.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wirecard.challenge.domain.Card;


public interface CardRepository extends JpaRepository<Card, Integer>{

	public Optional<Card> findFirstByNumber(String number);
}

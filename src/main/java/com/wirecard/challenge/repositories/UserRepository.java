package com.wirecard.challenge.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.wirecard.challenge.domain.Usuario;
import com.wirecard.challenge.domain.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Integer> {

	@Transactional(readOnly=true)
	Usuario findByUsername(String username);
	
	@Transactional(readOnly=true)
	Optional<Usuario> findByClientId(Integer id);
}

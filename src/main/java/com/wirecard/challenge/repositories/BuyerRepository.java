package com.wirecard.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wirecard.challenge.domain.Buyer;


public interface BuyerRepository extends JpaRepository<Buyer, Integer> {
}

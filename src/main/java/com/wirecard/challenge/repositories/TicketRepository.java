package com.wirecard.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wirecard.challenge.domain.Ticket;


public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}

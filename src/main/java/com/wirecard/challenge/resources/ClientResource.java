package com.wirecard.challenge.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.repositories.ClientRepository;
import com.wirecard.challenge.services.exceptions.WireCardException;

@RestController
@RequestMapping(value = "/clients")
public class ClientResource {

	@Autowired
	private ClientRepository clientRepository;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Client> listAll() throws WireCardException{
		return clientRepository.findAll();
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Client>> paymentStatus(@PathVariable("id") Integer id) throws WireCardException {
		
		Optional<Client> client = clientRepository.findById(id);
		
		if(!client.isPresent())
			return ResponseEntity.notFound().build();
		
		return ResponseEntity.ok().body(client);
	}
	
}

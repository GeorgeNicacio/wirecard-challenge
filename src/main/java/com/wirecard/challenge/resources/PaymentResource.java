package com.wirecard.challenge.resources;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.security.UserSS;
import com.wirecard.challenge.services.PaymentService;
import com.wirecard.challenge.services.UserService;
import com.wirecard.challenge.services.exceptions.BadRequestException;
import com.wirecard.challenge.services.exceptions.WireCardException;

@RestController
@RequestMapping(value = "/payments")
public class PaymentResource {

	@Autowired
	private PaymentService paymentService;
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.GET)
	public List<Payment> listAll() throws WireCardException{
		return paymentService.listAllPayments();
	}
	
	@PreAuthorize("hasAnyRole('CLIENTE')")
	@RequestMapping(value="/my", method = RequestMethod.GET)
	public ResponseEntity<List<Payment>> listAllMyPayments() throws WireCardException{

		UserSS user = UserService.authenticated();
		
		if(user == null)
			throw new BadRequestException();
			
		return ResponseEntity.ok().body(paymentService.listAllMyPayments(user.getUsername()));
			
	}

	@PreAuthorize("hasAnyRole('CLIENTE')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> paymentGenerate(@Valid @RequestBody Payment payment, BindingResult result) throws WireCardException {
		if (result.hasErrors()) {
			return ResponseEntity.badRequest().body(new BadRequestException().getMessage());
		}
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(payment.getId()).toUri();
		Optional<String> response = paymentService.createPayment(payment);

		if(response.isPresent())
			return ResponseEntity.created(uri).body(new Gson().toJson(response.get()));
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{payment_id}", method=RequestMethod.GET)
	public Payment paymentStatus(@PathVariable("payment_id") String payment_id) throws WireCardException {
		return paymentService.findPaymentById(payment_id);
	}
	
	
}

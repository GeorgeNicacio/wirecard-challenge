package com.wirecard.challenge.services;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.wirecard.challenge.domain.Ticket;
import com.wirecard.challenge.domain.Buyer;
import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.domain.Usuario;
import com.wirecard.challenge.domain.enums.Perfil;
import com.wirecard.challenge.repositories.TicketRepository;
import com.wirecard.challenge.repositories.BuyerRepository;
import com.wirecard.challenge.repositories.CardRepository;
import com.wirecard.challenge.repositories.ClientRepository;
import com.wirecard.challenge.repositories.PaymentRepository;
import com.wirecard.challenge.repositories.UserRepository;

@Service
public class DBService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private TicketRepository boletoRepository;
	
	@Autowired
	private BuyerRepository buyerRepository;
	
	@Autowired
	private CardRepository cardRepository;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;

	public void instantiateDatabase() throws ParseException {
		
		Client c = Client.builder()
				.id(null)
				.build();
	
		Client c2 = Client.builder()
				.id(null)
				.build();
		
		clientRepository.saveAll(Arrays.asList(c,c2));
			
		Usuario cli1 = Usuario.builder()
				.id(null)
				.username("georgenicacio")
				.password(pe.encode("123"))
				.client(c)
				.perfis(new HashSet<>())
				.build();
		cli1.addPerfil(Perfil.CLIENTE);
		
		Usuario cli2 = Usuario.builder()
				.id(null)
				.username("admin")
				.password(pe.encode("123"))
				.client(c2)
				.perfis(new HashSet<>())
				.build();
		cli2.addPerfil(Perfil.CLIENTE);
		cli2.addPerfil(Perfil.ADMIN);
		
		userRepository.saveAll(Arrays.asList(cli1, cli2));
		
		
		Ticket b = Ticket.builder()
				.id(null)
				.number("123013210321021")
				.build();
		
		Ticket b2 = Ticket.builder()
				.id(null)
				.number("123013210321022")
				.build();
				
		Ticket b3 = Ticket.builder()
				.id(null)
				.number("123013210321023")
				.build();
		
		Buyer bu = Buyer.builder()
				.cpf("26927963065")
				.email("georgenicacio@gmail.com")
				.id(null)
				.name("George Nicacio")
				.build();
		
		Buyer bu2 = Buyer.builder()
				.cpf("38355256034")
				.email("fulano2@gmail.com")
				.id(null)
				.name("Fulano 2")
				.build() ;
		
		Buyer bu3 = Buyer.builder()
				.cpf("52113674050")
				.email("fulano3@gmail.com")
				.id(null)
				.name("Fulano 3")
				.build() ;
		
		Ticket b4 = Ticket.builder()
				.id(null)
				.number("123013210321024")
				.build();
		
		Date dataDoUsuario = new Date();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataDoUsuario);
		calendar.add(Calendar.DATE, +1);
		
		dataDoUsuario = calendar.getTime();
		
		Card ca = Card.builder()
				.cvv("504")
				.expirationDate(dataDoUsuario)
				.holderName("FULANO DE TAL")
				.id(null)
				.number("5160365774786687")
				.build();
		Card ca2 = Card.builder()
				.cvv("246")
				.expirationDate(dataDoUsuario)
				.holderName("FULANO DE TAL 2")
				.id(null)
				.number("5490144361717293")
				.build();
		Card ca3 = Card.builder()
				.cvv("854")
				.expirationDate(dataDoUsuario)
				.holderName("FULANO DE TAL 3")
				.id(null)
				.number("5475112588183036")
				.build();
		
		cardRepository.saveAll(Arrays.asList(ca,ca2,ca3));
		
		Payment p = Payment.builder()
					.amount(new BigDecimal(150))
					.buyer(bu)
					.card(ca)
					.client(c)
					.id(null)
					.status(PaymentStatus.APPROVED)
					.type(PaymentType.CREDIT_CARD)
					.build();
	
		Payment p2 = Payment.builder()
				.amount(new BigDecimal(150))
				.buyer(bu2)
				.card(ca2)
				.client(c)
				.id(null)
				.status(PaymentStatus.CANCELED)
				.type(PaymentType.CREDIT_CARD)
				.build();
		
		Payment p3 = Payment.builder()
				.amount(new BigDecimal(150))
				.buyer(bu3)
				.card(ca3)
				.client(c)
				.id(null)
				.status(PaymentStatus.DENIED)
				.type(PaymentType.CREDIT_CARD)
				.build();
		
		Payment p4 = Payment.builder()
				.amount(new BigDecimal(150))
				.ticket(b4)
				.buyer(bu3)
				.client(c2)
				.id(null)
				.status(PaymentStatus.APPROVED)
				.type(PaymentType.TICKET)
				.build();
		
		paymentRepository.saveAll(Arrays.asList(p,p2,p3,p4));
		
	}
}

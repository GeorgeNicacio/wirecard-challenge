package com.wirecard.challenge.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.services.exceptions.ProcessorNotFoundException;
import com.wirecard.challenge.services.processor.PaymentProcessor;

@Service
public class ProcessorService {

    private Map<PaymentType, List<PaymentProcessor>> processors;

    @Autowired
    private ProcessorService(List<PaymentProcessor> allProcessors){
        this.processors = allProcessors.stream()
                .collect(Collectors.groupingBy(PaymentProcessor::getType));
    }

    public PaymentProcessor activePaymentProcessor(PaymentType type) throws ProcessorNotFoundException{
        return processors.get(type).stream()
        		.filter(p -> p.getStatus()).findAny()
        		.orElseThrow(() -> new ProcessorNotFoundException());
    }
}

package com.wirecard.challenge.services.exceptions;

import java.util.List;

import org.springframework.validation.ObjectError;

public class ProcessorNotFoundException extends WireCardException{
	private static final long serialVersionUID = 1L;

	public ProcessorNotFoundException() {
		super("Processor not implemented!");
	}

	public ProcessorNotFoundException(List<ObjectError> allErrors) {
		this();
		super.allErros = allErrors;
	}
}

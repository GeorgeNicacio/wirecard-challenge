package com.wirecard.challenge.services.exceptions;

public class BrandsUnknownException extends PaymentUnregisteredException {

	private static final long serialVersionUID = 1L;

	public BrandsUnknownException() {
		super("Unidentified card Brands");
	}
}

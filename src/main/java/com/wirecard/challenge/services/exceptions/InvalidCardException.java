package com.wirecard.challenge.services.exceptions;

public class InvalidCardException extends PaymentUnregisteredException {

	private static final long serialVersionUID = 1L;

	
	public InvalidCardException() {
		super("Invalid Card");
	}
}

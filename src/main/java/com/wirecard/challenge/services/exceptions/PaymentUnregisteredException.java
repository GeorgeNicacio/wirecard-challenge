package com.wirecard.challenge.services.exceptions;

import java.util.List;

import org.springframework.validation.ObjectError;

public class PaymentUnregisteredException extends WireCardException {

	private static final long serialVersionUID = 1L;

	public PaymentUnregisteredException(String message) {
		super(message);
	}
	
	public PaymentUnregisteredException() {
		super("Unregistered Payment!");
	}

	public PaymentUnregisteredException(List<ObjectError> allErrors) {
		this();
		super.allErros = allErrors;
	}
}

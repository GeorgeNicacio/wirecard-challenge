package com.wirecard.challenge.services.processor;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.repositories.CardRepository;
import com.wirecard.challenge.services.exceptions.BrandsUnknownException;
import com.wirecard.challenge.services.exceptions.InvalidCardException;
import com.wirecard.challenge.services.exceptions.PaymentUnregisteredException;

import br.com.moip.creditcard.Brands;
import br.com.moip.validators.CreditCard;

@Service
public class CreditCardProcessor extends PaymentProcessor {
	
	@Autowired
	private CardRepository cardRepository;
	
	@Override
	public Pair<Boolean, Optional<String>> process(Payment payment) throws PaymentUnregisteredException {
		try {
			payment.setCard(validateCard(payment.getCard()));
			payment.setStatus(PaymentStatus.APPROVED);
			paymentRepository.save(payment);
			return Pair.of(true, Optional.of(PaymentStatus.APPROVED.getDescription()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new PaymentUnregisteredException();
		}
	}

	private Card validateCard(Card card) throws PaymentUnregisteredException {
		CreditCard creditCard = new CreditCard(card.getNumber());
		
		if(creditCard.getBrand().equals(Brands.UNKNOWN)) {
			throw new BrandsUnknownException();
		}
		if(!creditCard.isValid()) {
			throw new InvalidCardException();
		}
		Optional<Card> cardPresent = cardRepository.findFirstByNumber(card.getNumber());
		if(cardPresent.isPresent()) {
			return cardPresent.get();
		}
		return cardRepository.save(card);
	}

	@Override
    public PaymentType getType() {
        return PaymentType.CREDIT_CARD;
	}

	@Override
	public boolean getStatus() {
		return true;
	}

}

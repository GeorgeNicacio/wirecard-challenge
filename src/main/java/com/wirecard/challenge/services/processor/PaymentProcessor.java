package com.wirecard.challenge.services.processor;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.repositories.PaymentRepository;
import com.wirecard.challenge.services.exceptions.PaymentUnregisteredException;


@Component
public abstract class PaymentProcessor {

	@Autowired
	protected PaymentRepository paymentRepository;
	
    public abstract Pair<Boolean, Optional<String>> process(Payment payment) throws PaymentUnregisteredException;
    public abstract PaymentType getType();
    public abstract boolean getStatus();

}

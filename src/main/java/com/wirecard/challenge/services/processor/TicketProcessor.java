package com.wirecard.challenge.services.processor;

import java.util.Optional;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import com.google.common.collect.Lists;
import com.wirecard.challenge.domain.Ticket;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;
import com.wirecard.challenge.domain.PaymentType;
import com.wirecard.challenge.services.exceptions.PaymentUnregisteredException;

@Service
public class TicketProcessor extends PaymentProcessor {

	@Override
	public Pair<Boolean, Optional<String>> process(Payment payment) throws PaymentUnregisteredException {
		payment.setTicket(Ticket.builder()
		.number("34191.09065 20674.712938 83456.900009 6 69240000029800")
		.build());
		try {
			payment.setStatus(PaymentStatus.APPROVED);
			paymentRepository.save(payment);
			return Pair.of(true, Optional.of(payment.getTicket().getNumber()));
		} catch (Exception e) {
			throw new PaymentUnregisteredException(
					Lists.newArrayList(new ObjectError("TICKET REGISTER", e.getMessage())));
		}
	}

	@Override
	public PaymentType getType() {
		return PaymentType.TICKET;
	}

	@Override
	public boolean getStatus() {
		return true;
	}

}

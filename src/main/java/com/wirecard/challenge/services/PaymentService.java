package com.wirecard.challenge.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import com.google.common.collect.Lists;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.repositories.PaymentRepository;
import com.wirecard.challenge.resources.exception.PaymentNotFoundException;
import com.wirecard.challenge.services.exceptions.BadRequestException;
import com.wirecard.challenge.services.exceptions.ObjectNotFoundException;
import com.wirecard.challenge.services.exceptions.PaymentUnregisteredException;
import com.wirecard.challenge.services.exceptions.ProcessorNotFoundException;
import com.wirecard.challenge.services.exceptions.WireCardException;

@Service
public class PaymentService {

	private ProcessorService processorSerice;
	private PaymentRepository paymentRepository;

	@Autowired
	public PaymentService(PaymentRepository paymentRepository, ProcessorService processorSerice) {
		this.processorSerice=processorSerice;
		this.paymentRepository=paymentRepository;
	}

	public Optional<String> createPayment(Payment payment) throws ProcessorNotFoundException, PaymentUnregisteredException {
		Pair<Boolean, Optional<String>> statusProcessing = processorSerice
				.activePaymentProcessor(payment.getType())
				.process(payment);
		if(statusProcessing.getFirst()) {
			return statusProcessing.getSecond();
		}
		throw new PaymentUnregisteredException();
	}

	public Payment findPaymentById(String payment_id) throws WireCardException{
		boolean isDigit = payment_id.chars().allMatch(Character::isDigit);
		if(!isDigit)
			throw new BadRequestException(
					Lists.newArrayList(new ObjectError(payment_id, "Field must be numeric")));
		
		return paymentRepository.findById(Integer.valueOf(payment_id))
				.orElseThrow(PaymentNotFoundException::new);
	}

	public List<Payment> listAllPayments() throws PaymentNotFoundException {
		List<Payment> payments = paymentRepository.findAll();
		if (payments.isEmpty())
			throw new PaymentNotFoundException();
		return payments;
	}
	
	public List<Payment> listAllMyPayments(String username) throws PaymentNotFoundException {
		
		if (username == null)
			throw new ObjectNotFoundException("Usuário não Encontrado!");
		
		List<Payment> payments = paymentRepository.findByUser(username);
		
		if (payments.isEmpty())
			throw new PaymentNotFoundException();
		
		return payments;
	}

}

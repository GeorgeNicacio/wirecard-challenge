package com.wirecard.challenge.domain;

public enum PaymentType {

	TICKET(1, "Ticket"), CREDIT_CARD(2, "Credit Card");

	private int code;
	private String description;

	PaymentType(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return this.code;
	}

	public String getDescription() {
		return this.description;
	}
}

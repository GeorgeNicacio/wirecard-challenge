# wirecard-challenge

## Estrutura do projeto

Os passos para obter o projeto em execução são os seguintes:

```
$ git clone https://GeorgeNicacio@bitbucket.org/GeorgeNicacio/wirecard-challenge.git
$ cd wirecard-challenge
$ mvn package
$ java -cp target/challenge-0.0.1-SNAPSHOT.jar com.wirecard.challenge.WCApplication

```

Para os proximos passos, deve ser observado a porta configurada para o servidor, no perfil selecionado no appplication.properties:

application.properties:

```
spring.profiles.active=dev
...

```
application-dev.properties:

```
...
server.port=8090
...

```

### Autenticação

Existem dois tipos de Perfis para acessos aos dados da API, que são eles:

* ADMIN (todo admin também é cliente)
* CLIENTE

Realizar o login como cliente ou admin, salvar o token gerado para ser utilizado nas requisições seguintes:

Login ADMIN:

```
POST /login HTTP/1.1
Host: localhost:8090
cache-control: no-cache
Postman-Token: 71a1c209-6772-48a2-9b8e-1ea65faeebbb
{
    "email": "admin",
    "senha": "123"
  
}

```

Login CLIENTE:

```
POST /login HTTP/1.1
Host: localhost:8090
cache-control: no-cache
Postman-Token: 71a1c209-6772-48a2-9b8e-1ea65faeebbb
{
    "email": "georgenicacio",
    "senha": "123"
  
}

```

### Clientes (A listagem de todos os clientes é restrito somente ao Perfil de Admin)

Os dados dos clientes podem ser obtidos atravÃ©s da seguinte uri:

```
GET /clients HTTP/1.1
Host: localhost:8090
Authorization: Bearer [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: 7c6925a7-4fa4-470e-9cdd-24b8a85d2468

=========================
Output: 
[
  {
    "id": 1
  }
...]

```

### Pagamentos

Os dados de pagamentos podem ser obtidos atravÃ©s da seguinte uri:

#### Listagem de Pagamentos (A listagem de todos os pagamentos é restrito somente ao Perfil de Admin)

```
GET /payments HTTP/1.1
Host: localhost:8090
Authorization: Bearer [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: a5f8bdca-ce7d-4a40-b6ef-e80042036700

=========================
Output: 
[
    {
        "id": 8,
        "amount": 150,
        "type": "CREDIT_CARD",
        "status": "APPROVED",
        "client": {
            "id": 1
        },
        "buyer": {
            "name": "George Nicacio",
            "email": "georgenicacio@gmail.com",
            "cpf": "26927963065"
        },
        "card": {
            "id": 5,
            "holderName": "FULANO DE TAL",
            "number": "5160365774786687",
            "expirationDate": "2019-04-13",
            "cvv": "504"
        },
        "boleto": null
    },
    {
        "id": 10,
        "amount": 150,
        "type": "CREDIT_CARD",
        "status": "CANCELED",
        "client": {
            "id": 1
        },
        "buyer": {
            "name": "Fulano 2",
            "email": "fulano2@gmail.com",
            "cpf": "38355256034"
        },
        "card": {
            "id": 6,
            "holderName": "FULANO DE TAL 2",
            "number": "5490144361717293",
            "expirationDate": "2019-04-13",
            "cvv": "246"
        },
        "boleto": null
    },
...]
```

#### Listagem de Pagamento Especifico (A listagem de pagamento especifico é restrito somente ao Perfil de Admin)

```
GET /payments/8 HTTP/1.1
Host: localhost:8090
Authorization: [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: 557bbeab-0c07-4c97-bb9a-d43ff3b40e19
{
"client": {
  "id": 1
},
"card": {
	"holderName": "FULANO DE TAL",
	"number": "5160365774786687",
	"expirationDate": "2019-12-04",
	"cvv": 504
},
"buyer": {
  "name": "George",
  "email": "georgenicacio@gmail.com",
  "cpf": "26927963065"
},
"amount": 500.00,
"type": "CREDIT_CARD"
}
==============================================
Output: 
[
{
    "id": 8,
    "amount": 150,
    "type": "CREDIT_CARD",
    "status": "APPROVED",
    "client": {
        "id": 1
    },
    "buyer": {
        "name": "George Nicacio",
        "email": "georgenicacio@gmail.com",
        "cpf": "26927963065"
    },
    "card": {
        "id": 5,
        "holderName": "FULANO DE TAL",
        "number": "5160365774786687",
        "expirationDate": "2019-04-13",
        "cvv": "504"
    },
    "boleto": null
}
]

```

#### Listagem de Meus Pagamentos

```
GET /payments/my/2 HTTP/1.1
Host: localhost:8090
Authorization: [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: 7a153a24-0b21-4c20-8119-c481af54359c
{
"client": {
  "id": 1
},
"card": {
	"holderName": "FULANO DE TAL",
	"number": "5160365774786687",
	"expirationDate": "2019-12-04",
	"cvv": 504
},
"buyer": {
  "name": "George",
  "email": "georgenicacio@gmail.com",
  "cpf": "26927963065"
},
"amount": 500.00,
"type": "CREDIT_CARD"
}
==============================================
Output: 
[
{
    "id": 14,
    "amount": 150,
    "type": "BOLETO",
    "status": "APPROVED",
    "client": {
        "id": 2
    },
    "buyer": {
        "name": "Fulano 3",
        "email": "fulano3@gmail.com",
        "cpf": "52113674050"
    },
    "card": null,
    "boleto": {
        "id": 15,
        "number": "123013210321024"
    }
}
...]

```

#### Cadastro de Pagamentos(BOLETO)
```
POST /payments HTTP/1.1
Host: localhost:8090
Authorization: Bearer [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: 8be18f32-63f5-479f-8b7f-d53118da333f
{
  "amount": 100.00,
  "buyer": {
    "cpf": "26927963065",
    "email": "georgenicacio@email.com",
    "name": "George"
  },
  "client": {
    "id": 1
  },
  "type": "BOLETO"
}
```
#### Cadastro de Pagamentos(CREDIT_CARD)
```
POST /payments HTTP/1.1
Host: localhost:8090
Authorization: Bearer [Token Gerado no passo de Autenticação]
cache-control: no-cache
Postman-Token: 79cacf13-62c5-4d2f-861a-65629d2d3d73
{
"client": {
  "id": 1
},
"card": {
	"holderName": "FULANO DE TAL",
	"number": "5160365774786687",
	"expirationDate": "2019-12-04",
	"cvv": 504
},
"buyer": {
  "name": "George",
  "email": "georgenicacio@gmail.com",
  "cpf": "26927963065"
},
"amount": 500.00,
"type": "CREDIT_CARD"
}
```

Tecnologias Utilizadas:

* Spring Boot 2.0
* Java 8
* Lombok
* H2 DataBase( Pode ser utilizado qualquer DataBase(Modificar o pom.xml para o jdbc utilizado), modificando as configurações de conexão do perfil informando no application. No projeto existe a classe DBService, que é responsável por dar a carga inicial no banco configurado.)

Documentação:
* Swagger 2
